--DDL
--CREATE
CREATE TABLE library (
id number primary key,
name varchar2 (50),
email varchar2 (100));
--ALTER
ALTER TABLE library ADD phone number; 
--DROP
DROP TABLE library;
--CONSTRAINTS
ALTER TABLE library
ADD CONSTRAINT PK_library PRIMARY KEY (id); 
--KEYS
ALTER TABLE library
ADD CONSTRAINT UQ_library_email UNIQUE (email);
