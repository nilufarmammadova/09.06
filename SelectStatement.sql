--SELECT STATEMENT
--PROJECTION CLAUSE
SELECT employee_id, first_name, email FROM employees;
--FROM CLAUSE
SELECT * FROM employees;
--FILTER CLAUSE
SELECT * FROM employees
WHERE salary > 5000;
--GROUP BY
SELECT employee_id, count (*) FROM employees
GROUP BY employee_id;
--HAVING CLAUSE
SELECT job_id, count (*) FROM employees
GROUP BY job_id
HAVING count(*) > 3;
--ORDER CLAUSE
SELECT email, hire_date FROM employees
ORDER BY hire_date DESC;
--FETCH CLAUSE
SELECT * FROM employees
ORDER BY employee_id
FETCH NEXT 10 ROWS ONLY;
